import os

import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_heroku_binary_in_place(host):
    assert host.file('/tmp/heroku/bin/heroku').exists
