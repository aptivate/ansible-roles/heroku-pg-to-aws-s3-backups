[![pipeline status](https://git.coop/aptivate/ansible-roles/heroku-pg-to-aws-s3-backups/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/heroku-pg-to-aws-s3-backups/commits/master)

# heroku-pg-to-aws-s3-backups

A role for moving daily [Heroku postgresql dumps] off-site to [AWS S3].

[Heroku postgresql dumps]: https://devcenter.heroku.com/articles/heroku-postgres-backups
[AWS S3]: https://aws.amazon.com/s3/

# Requirements

* [boto](https://pypi.org/project/boto/)
* [boto3](https://pypi.org/project/boto3/)

# Role Variables

* `herokupgs3_bucket_name`
  * AWS S3 bucket name.

* `herokupgs3_heroku_app_name`
  * Heroku application name.

You must also have the following exposed in your environment:

* `HEROKU_API_KEY`: Heroku API key.
* `AWS_ACCESS_KEY`: AWS S3 access key.
* `AWS_SECRET_ACCESS_KEY`: AWS S3 secret access key.

Make sure to check [defaults/main.yml](defaults/main.yml) for defaults.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  connection: local
  environment:
    AWS_ACCESS_KEY: !vault | ...
    AWS_SECRET_ACCESS_KEY: !vault | ...
    HEROKU_API_KEY: !vault | ...
  roles:
     - role: heroku-pg-to-aws-s3-backups
       herokupgs3_bucket_name: barfoo
       herokupgs3_heroku_app_name: foobar
```

# Testing

```bash
$ curl -sSL https://get.docker.com/ | sh  # install docker
$ pipenv run molecule test
```

# License

* https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

* http://aptivate.org/
* http://git.coop/aptivate
